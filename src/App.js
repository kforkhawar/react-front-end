import React, { Component } from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import Employees from "./components/employees";
import NotFound from "./components/notFound";
import NavBar from "./components/navBar";
import EmployeeForm from "./components/employeeForm";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";

class App extends Component {
  state = {};

  componentDidMount() {
  }

  render() {
    const { user } = this.state;

    return (
      <React.Fragment>
        <ToastContainer />
        <NavBar />
        <main className="container">
          <Switch>
            <Route path="/employee" component={EmployeeForm} />
            <Route
              path="/employees"
              component={Employees}
            />
            <Route path="/not-found" component={NotFound} />
            <Redirect from="/" exact to="/employees" />
            <Redirect to="/not-found" />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
