import React from "react";
import Joi from "joi-browser";
import Form from './common/form';
import { saveEmployee } from "../services/employeeService";
import { getDepartments } from "../services/departmentService";
import { toast } from "react-toastify";

class EmployeeForm extends Form {
    state = {
        data: {
            firstName: "",
            lastName: "",
            email: "",
            phoneNumber: "",
            salary: "",
            department: "",
            hireDate: "",
        },
        departments: [],
        errors: {}
    };

    schema = {
        _id: Joi.string(),
        firstName: Joi.string()
            .required()
            .label("First Name")
            .min(2)
            .max(30),
        lastName: Joi.string()
            .required()
            .label("Last Name")
            .min(2)
            .max(30),
        department: Joi.string()
            .required()
            .label("Department"),
        email: Joi.string()
            .required()
            .email()
            .max(30)
            .label("Email"),
        phoneNumber: Joi.string()
            .required()
            .regex(/^([0-9-])*$/)
            .label("Phone No")
            .max(25),
        salary: Joi.number()
            .integer()
            .required()
            .min(1)
            .label("Salary"),
        hireDate: Joi.date()
            .required()
    };

    async populateDepartments() {
        const {data : departments} = await getDepartments();
        this.setState({ departments });
    }

    componentDidMount() {
        this.populateDepartments();
    }


    doSubmit = async () => {

        let selectedDepartment = {};
        const data = { ...this.state.data };
        if(this.state.data.department){
            selectedDepartment = this.state.departments.filter(m => m._id === parseInt(this.state.data.department))
            data["department"] = selectedDepartment[0];
        }else{
            data["department"] = null;
        }

        // Convert Salary to Integer
        if(this.state.data.salary){
            data["salary"] = parseInt(this.state.data.salary);
        }
        try{
            await saveEmployee(data);
            toast.success("Employee Record saved");
            this.props.history.push("/employees");    
        }catch(ex){
            if(ex.response && ex.response.status===400)
                toast.error("You have entered an Invalid Input");
        }
    };

    render() {
        return (<div>
            <h1>Employee Form</h1>
            <form onSubmit={this.handleSubmit}>
                {this.renderInput("firstName", "First Name")}
                {this.renderInput("lastName", "Last Name")}
                {this.renderSelect("department", "Departments", this.state.departments)}
                {this.renderInput("email", "Email")}
                {this.renderInput("phoneNumber", "Phone Number")}
                {this.renderDate("hireDate", "Hire Date")}
                {this.renderInput("salary", "Salary")}
                {this.renderButton("Save")}
            </form>
        </div>);
    }
}

export default EmployeeForm;