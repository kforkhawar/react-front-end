import React, { Component } from "react";
import EmployeeTable from "./employeeTable";
import ListGroup from "./common/listGroup";
import Pagination from "./common/pagination";
import {getEmployees,saveEmployee} from "../services/employeeService";
import { getDepartments } from "../services/departmentService";
import { paginate } from "../utils/paginate";
import _ from "lodash";

class Employees extends Component {
  state = {
    employees: [],
    departments: [],
    currentPage: 1,
    pageSize: 5,
    searchQuery: "",
    selectDepartment: null,
    sortColumn: { path: "title", order: "asc" }
  };

  async componentDidMount() {
    const {data} = await getDepartments();
    const departments = [{ _id: "", name: "All Departments" }, ...data];

    const {data : employees} = await getEmployees();
    employees.forEach(function(obj) { 
      if(obj.hireDate)
        obj.hireDate = obj.hireDate.substring(0, 10); 
    });
    this.setState({ employees, departments });
  }


  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  handleDepartmentSelect = department => {
    this.setState({ selectDepartment: department, searchQuery: "", currentPage: 1 });
  };

  handleSearch = query => {
    this.setState({ searchQuery: query, selectDepartment: null, currentPage: 1 });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  getPagedData = () => {
    const {
      pageSize,
      currentPage,
      sortColumn,
      selectDepartment,
      searchQuery,
      employees: allEmployees
    } = this.state;

    let filtered = allEmployees;

    if (searchQuery)
      filtered = allEmployees.filter(m =>
        m.title.toLowerCase().startsWith(searchQuery.toLowerCase())
      );
    else if (selectDepartment && selectDepartment._id)
      filtered = allEmployees.filter(m => m.department && m.department._id === selectDepartment._id);

    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    const employees = paginate(sorted, currentPage, pageSize);

    return { totalCount: filtered.length, data: employees };
  };

  render() {
    const { length: count } = this.state.employees;
    const { pageSize, currentPage, sortColumn, searchQuery } = this.state;

    if (count === 0) return <p>There are no employees in the database.</p>;

    const { totalCount, data: employees } = this.getPagedData();

    return (
      <div className="row">
        <div className="col-2">
          <ListGroup
            items={this.state.departments}
            selectedItem={this.state.selectDepartment}
            onItemSelect={this.handleDepartmentSelect}
          />
        </div>
        <div className="col">
          <p>Showing {totalCount} Employees in the database.</p>

          <EmployeeTable
            employees={employees}
            sortColumn={sortColumn}
            onSort={this.handleSort}
          />
          <Pagination
            itemsCount={totalCount}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>
      </div>
    );
  }
}

export default Employees;
