import React, { Component } from "react";
import Table from "./common/table";

class EmployeeTable extends Component {
  columns = [
    {
      path: "firstName",
      label: "First Name",
    },
    {
      path: "lastName",
      label: "Last Name",
    },
    {
      path: "email",
      label: "Email",
    },
    {
      path: "phoneNumber",
      label: "Phone No",
    },
    {
      path: "salary",
      label: "Salary",
    },
    {
      path: "hireDate",
      label: "Hire Date",
    },
    {
      path: "department.name",
      label: "Department",
    }
  ];

 

  render() {
    const { employees, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={employees}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default EmployeeTable;
