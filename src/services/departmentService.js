import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/departments";

export function getDepartments() {
    return http.get(apiEndpoint);
}
