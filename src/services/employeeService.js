import http from "./httpService";
import { apiUrl } from "../config.json";


const apiEndpoint = apiUrl + "/employee";



export function getEmployees() {
    return http.get(apiEndpoint);
}


export function saveEmployee(employee) {
    return http.post(apiEndpoint, employee);
}